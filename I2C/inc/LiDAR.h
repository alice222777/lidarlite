#ifndef  FILE_LIDAR_H
#define FILE_LIDAR_H

#include "main.h"

#define LiDAR_DEFAULT_ADDRESS 0xC4 // 0x62 <<1

/* for "LiDAR_begin" */
#define CONFIG_Default 0
#define CONFIG_HighSpeed 1
#define CONFIG_LL 2
#define CONFIG_HH 3
#define I2CDir_Write 0
#define I2CDir_Read 1
#define I2CDir_Stop 2

// extern uint32_t mon_i2c2_ISR;
// extern uint32_t mon_i2c1_ISR;

/////////////////////////////////////////////////////////////////////////////////////////////
typedef enum {
        I2C_State_Idle = 0,

        I2C_State_Lidar1GeneratedStartWriteandAddr,
        I2C_State_Lidar1SendingCMD,
        I2C_State_Lidar1GeneratedFirstStop,
        I2C_State_Lidar1GeneratedStartRead,
        I2C_State_Lidar1ReceivedFirstByte,
        I2C_State_Lidar1ReceivedSecondByte,

        I2C_State_Lidar2GeneratedStartWriteandAddr,
        I2C_State_Lidar2SendingCMD,
        I2C_State_Lidar2GeneratedFirstStop,
        I2C_State_Lidar2GeneratedStartRead,
        I2C_State_Lidar2ReceivedFirstByte,
        I2C_State_Lidar2ReceivedSecondByte,
        
        I2C_State_Lidar3GeneratedStartWriteandAddr,
        I2C_State_Lidar3SendingCMD,
        I2C_State_Lidar3GeneratedFirstStop,
        I2C_State_Lidar3GeneratedStartRead,
        I2C_State_Lidar3ReceivedFirstByte,
        I2C_State_Lidar3ReceivedSecondByte,
        
        I2C_State_Lidar4GeneratedStartWriteandAddr,
        I2C_State_Lidar4SendingCMD,
        I2C_State_Lidar4GeneratedFirstStop,
        I2C_State_Lidar4GeneratedStartRead,
        I2C_State_Lidar4ReceivedFirstByte,
        I2C_State_Lidar4ReceivedSecondByte,
        
        I2C_State_Lidar5GeneratedStartWriteandAddr,
        I2C_State_Lidar5SendingCMD,
        I2C_State_Lidar5GeneratedFirstStop,
        I2C_State_Lidar5GeneratedStartRead,
        I2C_State_Lidar5ReceivedFirstByte,
        I2C_State_Lidar5ReceivedSecondByte,
        
        I2C_State_Lidar6GeneratedStartWriteandAddr,
        I2C_State_Lidar6SendingCMD,
        I2C_State_Lidar6GeneratedFirstStop,
        I2C_State_Lidar6GeneratedStartRead,
        I2C_State_Lidar6ReceivedFirstByte,
        I2C_State_Lidar6ReceivedSecondByte,
        
        I2C_State_Lidar7GeneratedStartWriteandAddr,
        I2C_State_Lidar7SendingCMD,
        I2C_State_Lidar7GeneratedFirstStop,
        I2C_State_Lidar7GeneratedStartRead,
        I2C_State_Lidar7ReceivedFirstByte,
        I2C_State_Lidar7ReceivedSecondByte,
        
        I2C_State_Lidar8GeneratedStartWriteandAddr,
        I2C_State_Lidar8SendingCMD,
        I2C_State_Lidar8GeneratedFirstStop,
        I2C_State_Lidar8GeneratedStartRead,
        I2C_State_Lidar8ReceivedFirstByte,
        I2C_State_Lidar8ReceivedSecondByte,

        I2C_State_Error
}I2C_ServiceState;
/////////////////////////////////////////////////////////////////////////////////////////////

typedef struct 
{
            uint32_t TimeStamp;
            float angle;
            uint16_t distance;
} LidarData_t;

typedef struct 
{
            // uint32_t TimeStamp;
            // float angle;
            // uint16_t distance;
            LidarData_t Data;
            uint8_t Addr;
            uint8_t DataReady;
} LiDAR_t;

extern LiDAR_t LiDAR_1;
extern LiDAR_t LiDAR_2;
extern LiDAR_t LiDAR_3;
extern LiDAR_t LiDAR_4;
extern LiDAR_t LiDAR_5;
extern LiDAR_t LiDAR_6;
extern LiDAR_t LiDAR_7;
extern LiDAR_t LiDAR_8;
// extern LiDAR_t LiDAR_test;

void I2C2_Configuration(void);
void I2C1_Configuration(void);

uint8_t LiDAR_SetAddr(uint8_t Addr);
void LidarAddr_Configuration(void);

void LiDAR_PwrEn_Configuration(void);
void LiDAR_Mode_Configuration(void);

void LiDAR_WriteByte(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr, uint8_t data);
uint8_t LiDAR_ReadByte(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr);
uint16_t LiDAR_Read2Bytes(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr);

void LiDAR_begin(I2C_TypeDef* I2cNum, uint8_t LidarAddr, int config);
void LiDAR_beginContinuous(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t num, uint8_t interval);

void LiDAR_ChangeAddress(I2C_TypeDef* I2cNum, uint8_t CurrentAddr, uint8_t NewAddr);
void LiDAR_ChangeMultiAddress(I2C_TypeDef* I2cNum);

int LiDAR_distance(I2C_TypeDef* I2cNum, uint8_t LidarAddr);
// void LiDAR_1_distanceContinuous(void);
// void LiDAR_2_distanceContinuous(void);
// void LiDAR_3_distanceContinuous(void);
// void LiDAR_4_distanceContinuous(void);

void EXTI4_15_IRQHandler(void);
void I2C2_IRQHandler(void);
void I2C1_IRQHandler(void);

void LiDAR1_Trigger_tranmission(void);
void LiDAR2_Trigger_tranmission(void);
void LiDAR3_Trigger_tranmission(void);
void LiDAR4_Trigger_tranmission(void);
void LiDAR5_Trigger_tranmission(void);
void LiDAR6_Trigger_tranmission(void);
void LiDAR7_Trigger_tranmission(void);
void LiDAR8_Trigger_tranmission(void);
I2C_ServiceState get_i2c_service_state(I2C_TypeDef* I2cNum);


#endif