// # define CounterPWM TIM2->CCR2
#ifndef  FILE_ECM1418_H
#define FILE_ECM1418_H

# include "LiDAR.h"
#include "main.h"

typedef struct {
	int step;
	char z;
	int dir;
} ECM1418_t;

extern ECM1418_t ECM1418;
extern uint16_t RoundCounter;

void ECM1418_Configuration(void);
void EXTI0_1_IRQHandler(void);
void EXTI2_3_IRQHandler(void);
float GetEncoderAngle(uint32_t TimeStamp);
// float ECM1418_GetAngle(LiDAR_t* LiDAR);
void TIM2_Configuration(void);
void TIM2_IRQHandler(void);

#endif