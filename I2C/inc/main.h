#ifndef  FILE_MAIN_H
#define FILE_MAIN_H

# include "stm32f0xx_conf.h"
# include "ecm1418.h"
# include "usart.h"
# include "LiDAR.h"
# include "spi.h"

// Package Selection -----------------------------------------------------------------------------------------

# define Package0xAA 0 	// TimeStamp, angle, distance
# define Package0xAB 0 	// IMU data
# define Package0xAC 1 	// Lidar ID, angle, distance
# define Package0xAD 0	// Lidar ID, TimeStamp, angle, distance
# define PrintSerialData 0
# define SendPointNum0xAE 0

// extern int counter;
// extern int mon;

# if Package0xAA
	extern uint8_t buffer_uart_Lidar[14];
# endif
# if Package0xAB
	extern uint8_t buffer_uart_imu[16];
# endif
# if Package0xAC
	extern uint8_t buffer_uart_Lidar2[11];
# endif
# if Package0xAD
	extern uint8_t buffer_uart_Lidar_0xAD[15];
# endif
#if SendPointNum0xAE
	extern uint16_t PointNum;
	extern uint8_t buffer_uart_PointNum_0xAE[6];
	extern uint8_t Get_SendPointNum0xAEChkSum(void);
#endif
extern int buffer_uart[100];

// extern int buffer_uart_ECM1418[100];
// extern int TimeStampPrintFlag;

void Delay_1us(uint32_t nCnt_1us);
void LED_Configuration(void);
uint8_t Get_ImuDataChkSum(void);
uint8_t Get_LidarDataChkSum(void);
uint8_t Get_LidarData2ChkSum(void);
uint8_t Get_LidarData0xADChkSum(void);
uint8_t Get_SendPointNum0xAEChkSum(void);
// void FreeGPIO_Configuration(void);

#endif