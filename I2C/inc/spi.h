#ifndef FILE_SPI_H
#define FILE_SPI_H

#include "main.h"

extern uint8_t spi1_buffer[25];
extern uint8_t uart_from_spi1_buffer[25];
extern uint8_t dma_spi1_complete_flag;
extern uint8_t spi1_RequestAddr[];
extern uint8_t spi2_buffer[25];
extern uint8_t uart_from_spi2_buffer[25];
extern uint8_t dma_spi2_complete_flag;
extern uint8_t spi2_RequestAddr[];
extern uint8_t WHOAMI[];
extern uint8_t ACCEL_X[];
extern uint8_t ACCEL_Y[];
extern uint8_t ACCEL_Z[];
extern uint8_t GYRO_X[];
extern uint8_t GYRO_Y[];
extern uint8_t GYRO_Z[];
extern uint8_t TEMP[];
extern uint16_t IMUData;

typedef struct 
{
	uint16_t ACCEL_X;
	uint16_t ACCEL_Y;
	uint16_t ACCEL_Z;
	uint16_t GYRO_X;
	uint16_t GYRO_Y;
	uint16_t GYRO_Z;
	// uint16_t TEMP;
} IMU_t;

extern IMU_t IMU;

uint8_t SPI_xfer(SPI_TypeDef *SPIx, uint8_t  WriteByte);
uint16_t SPI2_Get2bytes(uint8_t *AddrArray);
void SPI1_Configuration(void);
void spi1_dma_enable_receive_fast(uint8_t *buffer_addr);
void spi1_dma_enable_receive(uint8_t *buffer_addr);
void spi1_dma_transmit_fast(uint8_t *buffer_addr);
void spi1_dma_transmit(uint8_t *buffer_addr);
void SPI2_Configuration(void);
void spi2_dma_enable_receive_fast(uint8_t *buffer_addr);
void spi2_dma_enable_receive(uint8_t *buffer_addr);
void spi2_dma_transmit_fast(uint8_t *buffer_addr);
void spi2_dma_transmit(uint8_t *buffer_addr);

void DMA1_Channel2_3_IRQHandler(void);
void DMA1_Channel4_5_6_7_IRQHandler(void);

void spi1_Trigger_transmission(uint8_t *buffer_addr);
void spi2_GetIMUData(uint8_t *buffer_addr);

#endif
