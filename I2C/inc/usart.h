#ifndef FILE_USART_H
#define FILE_USART_H

#include <string.h>
#include "LiDAR.h"
#include "main.h"

void USART1_Initialization(void);
void USART3_Initialization(void);
void USART4_Initialization(void);
void USART1_IRQHandler(void);
void USART3_dma_init(void);
void usart3_dma_send_text(uint8_t *s);
# if Package0xAA
void usart3_dma_send_LidarData(uint8_t *s);
# endif
# if Package0xAC
void usart3_dma_send_LidarData2(uint8_t *s);
# endif
# if Package0xAD
void usart3_dma_send_LidarData_0xAD(uint8_t *s);
# endif
# if Package0xAB
void usart3_dma_send_ImuData(uint8_t *s);
# endif
# if SendPointNum0xAE
void usart3_dma_send_PointNum_0xAE(uint8_t *s);
# endif
void USART3_4_IRQHandler(void);

#endif
