#include "LiDAR.h"

/////////////////// NOTE ///////////////////////

// I2C2 : Lidar   1,   2,   5,   6,
// I2C1 : Lidar   7,   8,   3,   4,
// Pwn pin :    PB2, PA8, PA4, PA5,

////////////////////////////////////////////////

I2C_ServiceState i2c1_service_state = I2C_State_Idle;
I2C_ServiceState i2c2_service_state = I2C_State_Idle;
uint8_t lidar1_request_flag = 0;
uint8_t lidar2_request_flag = 0;
uint8_t lidar3_request_flag = 0;
uint8_t lidar4_request_flag = 0;
uint8_t lidar5_request_flag = 0;
uint8_t lidar6_request_flag = 0;
uint8_t lidar7_request_flag = 0;
uint8_t lidar8_request_flag = 0;
// uint32_t mon_i2c2_ISR = 0;
// uint32_t mon_i2c1_ISR = 0;
float AngleAdjustment_Lidar2 = 1.5;
float AngleAdjustment_Lidar3 = 0;
float AngleAdjustment_Lidar4 = 0;
float AngleAdjustment_Lidar5 = 0;
float AngleAdjustment_Lidar6 = 0;
float AngleAdjustment_Lidar7 = 0;
float AngleAdjustment_Lidar8 = 0;

/* LiDAR settings ===============================================================*/

LiDAR_t LiDAR_1 = { // I2C2
              .Addr = 0x10,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_2 = { // I2C2
              .Addr = 0x12,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_5 = { // I2C2
              .Addr = 0x14,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_6 = { // I2C2
              .Addr = 0x16,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_3 = { // I2C1
              .Addr = 0x20,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_4 = { // I2C1
              .Addr = 0x22,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_7 = { // I2C1
              .Addr = 0x24,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
LiDAR_t LiDAR_8 = { // I2C1
              .Addr = 0x26,
              .DataReady = 0,
              .Data = {
                            .distance = 0,
                            .angle = 0,
                            .TimeStamp = 0 }, };
// LiDAR_t LiDAR_test = {
//               .Addr = 0x30,
//               .DataReady = 0,
//               .Data = {
//                             .distance = 57,
//                             .angle = 163.6825,
//                             .TimeStamp = 1859375 }, };

/* ============================================================================ *
*  device mode : master
*  I2C speed mode : standard mode
*  I2C speed frequency (KHz) : 400
*  I2C clock source frequency (KHz) : 48000
*  Analog Filter Delay : ON
*  Coefficient of digital filter : 0
*  Rise time (ns) : 200
*  Fall time (ns) : 10
*/
void I2C2_Configuration(void)
{
    // RCC --------------------------------------------------------------------------------------
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);  
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    // GPIO ------------------------------------------------------------------------------------
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_1);

    GPIO_InitTypeDef GPIO_InitStructure = {
            .GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11,
            .GPIO_Mode = GPIO_Mode_AF,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_OType = GPIO_OType_OD,
    };
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // I2C ---------------------------------------------------------------------------------------
    I2C_InitTypeDef I2C_InitStructure = {

            // .I2C_Timing = 0x00901850,    // Fast mode, 400k, 48MHz
            // .I2C_Timing = 0x10900C22,    // Fast mode, 400k, 48MHz 300ns, 10ns
            .I2C_Timing = 0x00E01A4E,    // Fast mode, 400k, 48MHz 200ns, 10ns
            // .I2C_Timing = 0x00501953,    // Fast mode, 400k, 48MHz 10ns, 10ns
            // .I2C_Timing = 0x00701735,    // Fast mode, 400k, 36MHz
            // .I2C_Timing = 0x0040131E,    // Fast mode, 400k, 24MHz

            // .I2C_Timing = 0x10400CDB,    // Fast mode, 100k, 48MHz
            // .I2C_Timing = 0x20200A68,    // Fast mode, 100k, 36MHz
            // .I2C_Timing = 0x00301D7A,    // Fast mode, 100k, 16MHz

            // .I2C_Timing = 0x10805E89,    // Std mode, 100k, 48MHz
            // .I2C_Timing = 0x00C08CCE,    // Std mode, 100k, 36MHz
            // .I2C_Timing = 0x00805C89,    // Std mode, 100k, 24MHz
            // .I2C_Timing = 0x00503D5A,    // Std mode, 100k, 16MHz
            // .I2C_Timing = 0x00503D58,    // Std mode, 100k, 16MHz 100ns, 100ns
            // .I2C_Timing = 0x00402D42,    // Std mode, 100k, 12MHz
            // .I2C_Timing = 0x00201D2B,    // Std mode, 100k, 8MHz
            // .I2C_Timing = 0x40000405,    // Std mode, 100k, 6MHz

            .I2C_AnalogFilter = I2C_AnalogFilter_Enable,
            .I2C_DigitalFilter = 0x00,  
            .I2C_Mode = I2C_Mode_I2C,
            .I2C_OwnAddress1 = 0x00,
            .I2C_Ack = I2C_Ack_Enable,
            .I2C_AcknowledgedAddress  = I2C_AcknowledgedAddress_7bit,
    };
    I2C_Init(I2C2, &I2C_InitStructure);

    // NVIC -------------------------------------------------------------------------------------
    NVIC_InitTypeDef NVIC_InitStructure = {
            .NVIC_IRQChannel = I2C2_IRQn,
            .NVIC_IRQChannelPriority = 0x00,
            .NVIC_IRQChannelCmd = ENABLE
    };
    NVIC_Init(&NVIC_InitStructure);
    I2C_ITConfig(I2C2, I2C_IT_TCI | I2C_IT_STOPI | I2C_IT_RXI | I2C_IT_TXI , DISABLE);
    I2C_Cmd(I2C2, ENABLE);
}

/* ============================================================================ */

void I2C1_Configuration(void)
{
    // RCC --------------------------------------------------------------------------------------
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);  
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);

    // GPIO ------------------------------------------------------------------------------------
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_1);

    GPIO_InitTypeDef GPIO_InitStructure = {
            .GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7,
            .GPIO_Mode = GPIO_Mode_AF,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_OType = GPIO_OType_OD,
    };
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    // I2C ---------------------------------------------------------------------------------------
    I2C_InitTypeDef I2C_InitStructure = {
            .I2C_Timing = 0x00E01A4E,
            .I2C_AnalogFilter = I2C_AnalogFilter_Enable,
            .I2C_DigitalFilter = 0x00,  
            .I2C_Mode = I2C_Mode_I2C,
            .I2C_OwnAddress1 = 0x00,
            .I2C_Ack = I2C_Ack_Enable,
            .I2C_AcknowledgedAddress  = I2C_AcknowledgedAddress_7bit,
    };
    I2C_Init(I2C1, &I2C_InitStructure);

    // NVIC -------------------------------------------------------------------------------------
    NVIC_InitTypeDef NVIC_InitStructure = {
            .NVIC_IRQChannel = I2C1_IRQn,
            .NVIC_IRQChannelPriority = 0x00,
            .NVIC_IRQChannelCmd = ENABLE
    };
    NVIC_Init(&NVIC_InitStructure);
    I2C_ITConfig(I2C1, I2C_IT_TCI | I2C_IT_STOPI | I2C_IT_RXI | I2C_IT_TXI , DISABLE);
    I2C_Cmd(I2C1, ENABLE);
}

/* ============================================================================ */

uint8_t LiDAR_SetAddr(uint8_t Addr)
{
  return Addr <<=1;
}

/* ============================================================================ */

void LidarAddr_Configuration(void)
{
    // I2C2 : Lidar 1, 2, 5, 6
    // I2C1 : Lidar 7, 8, 3, 4
  
    GPIO_ResetBits(GPIOB, GPIO_Pin_2);
    GPIO_ResetBits(GPIOA, GPIO_Pin_8); 
    GPIO_ResetBits(GPIOA, GPIO_Pin_4);
    GPIO_ResetBits(GPIOA, GPIO_Pin_5);
    Delay_1us(100000);

    GPIO_SetBits(GPIOB, GPIO_Pin_2);
    Delay_1us(50000);
    LiDAR_ChangeAddress(I2C1, LiDAR_DEFAULT_ADDRESS, LiDAR_8.Addr);
    LiDAR_ChangeAddress(I2C2, LiDAR_DEFAULT_ADDRESS, LiDAR_1.Addr);
    GPIO_SetBits(GPIOC, GPIO_Pin_9);

    GPIO_SetBits(GPIOA, GPIO_Pin_8);
    Delay_1us(50000);
    LiDAR_ChangeAddress(I2C2, LiDAR_DEFAULT_ADDRESS, LiDAR_2.Addr);
    LiDAR_ChangeAddress(I2C1, LiDAR_DEFAULT_ADDRESS, LiDAR_7.Addr);

    Delay_1us(50000);
    GPIO_SetBits(GPIOA, GPIO_Pin_4);
    Delay_1us(50000);
    LiDAR_ChangeAddress(I2C2, LiDAR_DEFAULT_ADDRESS, LiDAR_6.Addr);
    LiDAR_ChangeAddress(I2C1, LiDAR_DEFAULT_ADDRESS, LiDAR_3.Addr);

    GPIO_SetBits(GPIOA, GPIO_Pin_5);
    Delay_1us(50000);
    LiDAR_ChangeAddress(I2C2, LiDAR_DEFAULT_ADDRESS, LiDAR_5.Addr);
    LiDAR_ChangeAddress(I2C1, LiDAR_DEFAULT_ADDRESS, LiDAR_4.Addr);

    LiDAR_1.Addr = LiDAR_SetAddr(LiDAR_1.Addr);
    LiDAR_2.Addr = LiDAR_SetAddr(LiDAR_2.Addr);
    LiDAR_3.Addr = LiDAR_SetAddr(LiDAR_3.Addr);
    LiDAR_4.Addr = LiDAR_SetAddr(LiDAR_4.Addr);
    LiDAR_5.Addr = LiDAR_SetAddr(LiDAR_5.Addr);
    LiDAR_6.Addr = LiDAR_SetAddr(LiDAR_6.Addr);
    LiDAR_7.Addr = LiDAR_SetAddr(LiDAR_7.Addr);
    LiDAR_8.Addr = LiDAR_SetAddr(LiDAR_8.Addr);
}

/* ============================================================================ */

/* @brief : GPIO setting for LiDAR power enable pins. */
// PC3, PA1, PA4, PA5
// PB2, PA8, PA4, PA5 (new)

void LiDAR_PwrEn_Configuration(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  GPIO_InitTypeDef GPIO_InitStructure = {
      .GPIO_Pin = GPIO_Pin_2,
      .GPIO_Mode = GPIO_Mode_OUT,
      .GPIO_Speed = GPIO_Speed_50MHz,
      .GPIO_OType = GPIO_OType_PP,
      .GPIO_PuPd = GPIO_PuPd_NOPULL
  };
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_SetBits(GPIOB, GPIO_Pin_2);  // LiDAR 1 PwrEn
  GPIO_SetBits(GPIOA, GPIO_Pin_8);  // LiDAR 2 PwrEn
  GPIO_SetBits(GPIOA, GPIO_Pin_4);  // LiDAR 3 PwrEn
  GPIO_SetBits(GPIOA, GPIO_Pin_5);  // LiDAR 4 PwrEn
}

/* ============================================================================ */

/* @brief : settings for LiDAR mode pins. */
// Lidar 1 PA7
// Lidar 2 PC12
// Lidar 3 PB4
// Lidar 4 PA6
// Lidar 5 PC8
// Lidar 6 PA10
// Lidar 7 PA9
// Lidar 8 PA15
void LiDAR_Mode_Configuration(void)
{
  // RCC ----------------------------------------------------------------------------------------
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  // GPIO --------------------------------------------------------------------------------------
  GPIO_InitTypeDef GPIO_InitStructure = {
      .GPIO_Pin = GPIO_Pin_4,
      .GPIO_Mode = GPIO_Mode_IN,
      .GPIO_PuPd = GPIO_PuPd_NOPULL
  };
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_12;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_15;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, GPIO_PinSource7);   // Lidar 1 PA7
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, GPIO_PinSource12);  // Lidar 2 PC12
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, GPIO_PinSource4);   // Lidar 3 PB4
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, GPIO_PinSource6);   // Lidar 4 PA6
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, GPIO_PinSource8);   // Lidar 5 PC8
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, GPIO_PinSource10);  // Lidar 6 PA10
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, GPIO_PinSource9);   // Lidar 7 PA9
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, GPIO_PinSource15);  // Lidar 8 PA15

  // EXTI --------------------------------------------------------------------------------------
  EXTI_InitTypeDef EXTI_InitStructure = {
      .EXTI_Line = EXTI_Line4 | EXTI_Line6 | EXTI_Line7 | EXTI_Line8 | EXTI_Line9 | EXTI_Line10 | EXTI_Line12 | EXTI_Line15,
      .EXTI_Mode = EXTI_Mode_Interrupt,
      .EXTI_Trigger = EXTI_Trigger_Falling,
      .EXTI_LineCmd = ENABLE
  };
  EXTI_Init(&EXTI_InitStructure);

    // NVIC --------------------------------------------------------------------------------------
  NVIC_InitTypeDef NVIC_InitStructure = {
      .NVIC_IRQChannel = EXTI4_15_IRQn,
      .NVIC_IRQChannelPriority = 0x03,
      .NVIC_IRQChannelCmd = ENABLE
  };
  NVIC_Init(&NVIC_InitStructure);
}

/* ============================================================================ */

void LiDAR_WriteByte(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr, uint8_t data)
{
  // while(I2C_GetFlagStatus(I2cNum, I2C_FLAG_BUSY) != RESET);
  
  I2C_TransferHandling(I2cNum, LidarAddr , 2, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TXIS) == RESET);

  I2C_SendData(I2cNum, Addr);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TXIS) == RESET);

  I2C_SendData(I2cNum, data);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TC) == RESET);

  I2C_TransferHandling(I2cNum, LidarAddr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
  while(I2C_GetFlagStatus(I2cNum, I2C_ISR_STOPF) == RESET);

  I2C_ClearFlag(I2cNum, I2C_FLAG_STOPF);
}

/* ============================================================================ */

uint8_t LiDAR_ReadByte(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr)
{
  uint8_t data;

  // while(I2C_GetFlagStatus(I2cNum, I2C_FLAG_BUSY) != RESET);

  I2C_TransferHandling(I2cNum, LidarAddr , 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TXIS) == RESET);

  I2C_SendData(I2cNum, Addr);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TC) == RESET);

  I2C_TransferHandling(I2cNum, LidarAddr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
  while(I2C_GetFlagStatus(I2cNum, I2C_ISR_STOPF) == RESET);

  I2C_ClearFlag(I2cNum, I2C_FLAG_STOPF);

  I2C_TransferHandling(I2cNum, LidarAddr , 1, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_RXNE) == RESET);

  data = I2C_ReceiveData(I2cNum);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_STOPF) == RESET);

  I2C_ClearFlag(I2cNum, I2C_FLAG_STOPF);

  return data;
}

/* ============================================================================ */

uint16_t LiDAR_Read2Bytes(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t Addr)
{
  uint8_t data_high,data_low;
  uint16_t data;

  // while(I2C_GetFlagStatus(I2cNum, I2C_FLAG_BUSY) != RESET);

  I2C_TransferHandling(I2cNum, LidarAddr , 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
  // mon_i2c2_ISR = I2C2->ISR;
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TXIS) == RESET);

  I2C_SendData(I2cNum, Addr);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_TC) == RESET);

  I2C_TransferHandling(I2cNum, LidarAddr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_STOPF) == RESET);

  I2C_ClearFlag(I2cNum, I2C_FLAG_STOPF);

  I2C_TransferHandling(I2cNum, LidarAddr , 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_RXNE) == RESET);

  data_high = I2C_ReceiveData(I2cNum);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_RXNE) == RESET);

  data_low = I2C_ReceiveData(I2cNum);
  while (I2C_GetFlagStatus(I2cNum, I2C_FLAG_STOPF) == RESET);

  I2C_ClearFlag(I2cNum, I2C_FLAG_STOPF);
  
  return data = (data_high<<8)|data_low;
}

/* ============================================================================ */
/* @param config : 
*         This parameter can be one of the following values:
*         @arg CONFIG_Default : default configuration 
*         @arg CONFIG_HighSpeed : Set aquisition count to 1/3 default value, faster reads, slightly noisier values
*         @arg CONFIG_LL : Low noise, low sensitivity: Pulls decision criteria higher above the noise, allows fewer false  detections, reduces sensitivity
*         @arg CONFIG_HH : High noise, high sensitivity: Pulls decision criteria into the noise, allows more false detections, increses sensitivity
*/
void LiDAR_begin(I2C_TypeDef* I2cNum, uint8_t LidarAddr, int config)
{
    if(config == CONFIG_HighSpeed)
    {
        LiDAR_WriteByte(I2cNum, LidarAddr, 0x04, 0x00); 
    }
    else if(config == CONFIG_LL)
    {
        LiDAR_WriteByte(I2cNum, LidarAddr, 0x1C, 0x20);
    }
    else if(config == CONFIG_HH)
    {
        LiDAR_WriteByte(I2cNum, LidarAddr, 0x1C, 0x60);
    }
    else  // default mode
    {
        LiDAR_WriteByte(I2cNum, LidarAddr, 0x00, 0x00);
    }
}

/* ============================================================================ */

void LiDAR_beginContinuous(I2C_TypeDef* I2cNum, uint8_t LidarAddr, uint8_t num, uint8_t interval)
{
  //  Register 0x45 sets the time between measurements. 0xc8 corresponds to 10Hz
  //  while 0x13 corresponds to 100Hz. Minimum value is 0x02 for proper
  //  operation.
  LiDAR_WriteByte(I2cNum, LidarAddr, 0x45, interval);
  Delay_1us(5000);

  //  Set register 0x04 to 0x20 to look at "NON-default" value of velocity scale
  //  If you set bit 0 of 0x04 to "1" then the mode pin will be low when done
  LiDAR_WriteByte(I2cNum, LidarAddr, 0x04, 0x21);
  Delay_1us(5000);

  //  Set the number of readings, 0xfe = 254 readings, 0x01 = 1 reading and
  //  0xff = continuous readings
  LiDAR_WriteByte(I2cNum, LidarAddr, 0x11, num);
  Delay_1us(5000);

  //  Initiate reading distance
  LiDAR_WriteByte(I2cNum, LidarAddr, 0x00, 0x04);
  Delay_1us(5000);
}

/* ============================================================================ */

void LiDAR_ChangeAddress(I2C_TypeDef* I2cNum, uint8_t CurrentAddr, uint8_t NewAddr)
{
  uint16_t SerialNum;
  
  /*Read the two byte serial number from register 0x96 */
  SerialNum = LiDAR_Read2Bytes(I2cNum, CurrentAddr, 0x96);

  /* Write the low and high byte of the serial number to 0x18 , 0x19*/
  LiDAR_WriteByte(I2cNum, CurrentAddr, 0x18, (uint8_t)(SerialNum>>8));
  LiDAR_WriteByte(I2cNum, CurrentAddr, 0x19, (uint8_t)SerialNum);

  /* Write the new address you want to use to 0x1A */
  LiDAR_WriteByte(I2cNum, CurrentAddr, 0x1A, NewAddr);

  Delay_1us(20);  
  /* check the new addr */
  // the MSB of new address will be set as 0
  // ex. NewAddr = 0x82, then after this function, the value read from 0x1A = 0x02.
  while((LiDAR_ReadByte(I2cNum, CurrentAddr, 0x1A)) != (NewAddr&0x7F));
  
    /* Disable the old address */
    LiDAR_WriteByte(I2cNum, CurrentAddr, 0x1E, 0x08);
}

/* ============================================================================ */

void LiDAR_ChangeMultiAddress(I2C_TypeDef* I2cNum)
{  
        if (I2cNum == I2C2)   // LiDAR 1 and 2
        {
              GPIO_ResetBits(GPIOB, GPIO_Pin_2);
              GPIO_ResetBits(GPIOA, GPIO_Pin_8); 
              Delay_1us(100000);

              GPIO_SetBits(GPIOB, GPIO_Pin_2);
              Delay_1us(50000);
              LiDAR_ChangeAddress(I2cNum, LiDAR_DEFAULT_ADDRESS, LiDAR_1.Addr);

              GPIO_SetBits(GPIOA, GPIO_Pin_8);
              Delay_1us(50000);
              LiDAR_ChangeAddress(I2cNum, LiDAR_DEFAULT_ADDRESS, LiDAR_2.Addr);

              LiDAR_1.Addr = LiDAR_SetAddr(LiDAR_1.Addr);
              LiDAR_2.Addr = LiDAR_SetAddr(LiDAR_2.Addr);
        };

        if (I2cNum == I2C1)   // LiDAR 3 and 4
        {
              GPIO_ResetBits(GPIOA, GPIO_Pin_4);
              GPIO_ResetBits(GPIOA, GPIO_Pin_5);
              Delay_1us(100000);

              GPIO_SetBits(GPIOA, GPIO_Pin_4);
              Delay_1us(50000);
              LiDAR_ChangeAddress(I2cNum, LiDAR_DEFAULT_ADDRESS, LiDAR_3.Addr);

              GPIO_SetBits(GPIOA, GPIO_Pin_5);
              Delay_1us(50000);
              LiDAR_ChangeAddress(I2cNum, LiDAR_DEFAULT_ADDRESS, LiDAR_4.Addr);

              LiDAR_3.Addr = LiDAR_SetAddr(LiDAR_3.Addr);
              LiDAR_4.Addr = LiDAR_SetAddr(LiDAR_4.Addr);
        };
}

/* ============================================================================ */

int LiDAR_distance(I2C_TypeDef* I2cNum, uint8_t LidarAddr)
{
        uint16_t distance;

        /* odometry start */
        LiDAR_WriteByte(I2cNum, LidarAddr, 0x00, 0x04);

        Delay_1us(200);

        /* check if the LiDAR is busy or not */
        while((LiDAR_ReadByte(I2cNum, LidarAddr, 0x01)&0x01) == 0x01);

        distance = LiDAR_Read2Bytes(I2cNum, LidarAddr, 0x8f);

        return distance;
}

/* ============================================================================ */

// void LiDAR_1_distanceContinuous(void)
// {
//         while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7));
//         LiDAR_1.Data.distance = LiDAR_Read2Bytes(I2C2, LiDAR_1.Addr, 0x8F);
// }

// /* ============================================================================ */

// void LiDAR_2_distanceContinuous(void)
// {
//         while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12));
//         LiDAR_2.Data.distance = LiDAR_Read2Bytes(I2C2, LiDAR_2.Addr, 0x8F);
// }

// /* ============================================================================ */

// void LiDAR_3_distanceContinuous(void)
// {
//         while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_4));
//         LiDAR_3.Data.distance = LiDAR_Read2Bytes(I2C1, LiDAR_3.Addr, 0x8F);
// }

// /* ============================================================================ */

// void LiDAR_4_distanceContinuous(void)
// {
//         while(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6));
//         LiDAR_4.Data.distance = LiDAR_Read2Bytes(I2C1, LiDAR_4.Addr, 0x8F);
// }

/* ============================================================================ */

void LiDAR1_Trigger_tranmission(void)
{
        i2c2_service_state = I2C_State_Lidar1GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C2, LiDAR_1.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR2_Trigger_tranmission(void)
{
        i2c2_service_state = I2C_State_Lidar2GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C2, LiDAR_2.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR3_Trigger_tranmission(void)
{
        i2c1_service_state = I2C_State_Lidar3GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C1, LiDAR_3.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR4_Trigger_tranmission(void)
{
        i2c1_service_state = I2C_State_Lidar4GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C1, LiDAR_4.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR5_Trigger_tranmission(void)
{
        i2c2_service_state = I2C_State_Lidar5GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C2, LiDAR_5.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR6_Trigger_tranmission(void)
{
        i2c2_service_state = I2C_State_Lidar6GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C2, LiDAR_6.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR7_Trigger_tranmission(void)
{
        i2c1_service_state = I2C_State_Lidar7GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C1, LiDAR_7.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

void LiDAR8_Trigger_tranmission(void)
{
        i2c1_service_state = I2C_State_Lidar8GeneratedStartWriteandAddr;
        I2C_TransferHandling(I2C1, LiDAR_8.Addr, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
}

/* ============================================================================ */

I2C_ServiceState get_i2c_service_state(I2C_TypeDef* I2cNum)
{
       if (I2cNum == I2C1)
       {
              return i2c1_service_state;
        }
        else if (I2cNum == I2C2)
        {
              return i2c2_service_state;
        }
        else 
        {
              return I2C_State_Error;
        }
}

/* ============================================================================ */
// PB5, PB4, PC12, PC11
// PA7, PC12, PB4, PA6 (new)

void EXTI4_15_IRQHandler(void)    // mode pin IRQHandler
{
       // LiDAR 1 ------------------------------------------------------------------------------------------------------------
        if (EXTI_GetFlagStatus(EXTI_Line7))
        {
              // 360 / 512 = 0.703125
              LiDAR_1.Data.TimeStamp = TIM_GetCounter(TIM2);
              // LiDAR_1.Data.angle = (float)ECM1418.step*0.703125;
              LiDAR_1.Data.angle = GetEncoderAngle(LiDAR_1.Data.TimeStamp);
              LiDAR_1.Data.angle = (LiDAR_1.Data.angle >= 360 )? LiDAR_1.Data.angle -=360 : LiDAR_1.Data.angle;
              
              if(get_i2c_service_state(I2C2) == I2C_State_Idle)
              {
                    LiDAR1_Trigger_tranmission();
              }
              else
              {
                    lidar1_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line7);
        }

        // LiDAR 2 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line12))
        {
              LiDAR_2.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_2.Data.angle = GetEncoderAngle(LiDAR_2.Data.TimeStamp) + 90 + AngleAdjustment_Lidar2;
              LiDAR_2.Data.angle = (LiDAR_2.Data.angle >= 360 )? LiDAR_2.Data.angle -=360 : LiDAR_2.Data.angle;
              
              if(get_i2c_service_state(I2C2) == I2C_State_Idle)
              {
                    LiDAR2_Trigger_tranmission();
              }
              else
              {
                    lidar2_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line12);
        }

        // LiDAR 3 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line4))
        {
              LiDAR_3.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_3.Data.angle = GetEncoderAngle(LiDAR_3.Data.TimeStamp) + 180 + AngleAdjustment_Lidar3;
              LiDAR_3.Data.angle = (LiDAR_3.Data.angle >= 360 )? LiDAR_3.Data.angle -=360 : LiDAR_3.Data.angle;
              
              if(get_i2c_service_state(I2C1) == I2C_State_Idle)
              {
                    LiDAR3_Trigger_tranmission();
              }
              else
              {
                    lidar3_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line4);
        }

        // LiDAR 4 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line6))
        {
              LiDAR_4.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_4.Data.angle = GetEncoderAngle(LiDAR_4.Data.TimeStamp) + 270 + AngleAdjustment_Lidar4;
              LiDAR_4.Data.angle = (LiDAR_4.Data.angle >= 360 )? LiDAR_4.Data.angle -=360 : LiDAR_4.Data.angle;
              
              if(get_i2c_service_state(I2C1) == I2C_State_Idle)
              {
                    LiDAR4_Trigger_tranmission();
              }
              else
              {
                    lidar4_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line6);
        }

        // LiDAR 5 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line8))
        {
              LiDAR_5.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_5.Data.angle = GetEncoderAngle(LiDAR_5.Data.TimeStamp) + 45 + AngleAdjustment_Lidar5;
              LiDAR_5.Data.angle = (LiDAR_5.Data.angle >= 360 )? LiDAR_5.Data.angle -=360 : LiDAR_5.Data.angle;
              
              if(get_i2c_service_state(I2C2) == I2C_State_Idle)
              {
                    LiDAR5_Trigger_tranmission();
              }
              else
              {
                    lidar5_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line8);
        }

        // LiDAR 6 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line10))
        {
              LiDAR_6.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_6.Data.angle = GetEncoderAngle(LiDAR_6.Data.TimeStamp) + 135 + AngleAdjustment_Lidar6;
              LiDAR_6.Data.angle = (LiDAR_6.Data.angle >= 360 )? LiDAR_6.Data.angle -=360 : LiDAR_6.Data.angle;
              
              if(get_i2c_service_state(I2C2) == I2C_State_Idle)
              {
                    LiDAR6_Trigger_tranmission();
              }
              else
              {
                    lidar6_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line10);
        }

        // LiDAR 7 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line9))
        {
              LiDAR_7.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_7.Data.angle = GetEncoderAngle(LiDAR_7.Data.TimeStamp) + 225 + AngleAdjustment_Lidar7;
              LiDAR_7.Data.angle = (LiDAR_7.Data.angle >= 360 )? LiDAR_7.Data.angle -=360 : LiDAR_7.Data.angle;
              
              if(get_i2c_service_state(I2C1) == I2C_State_Idle)
              {
                    LiDAR7_Trigger_tranmission();
              }
              else
              {
                    lidar7_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line9);
        }

        // LiDAR 8 ------------------------------------------------------------------------------------------------------------
        else if (EXTI_GetFlagStatus(EXTI_Line15))
        {
              LiDAR_8.Data.TimeStamp = TIM_GetCounter(TIM2);
              LiDAR_8.Data.angle = GetEncoderAngle(LiDAR_8.Data.TimeStamp) + 315 + AngleAdjustment_Lidar8;
              LiDAR_8.Data.angle = (LiDAR_8.Data.angle >= 360 )? LiDAR_8.Data.angle -=360 : LiDAR_8.Data.angle;
              
              if(get_i2c_service_state(I2C1) == I2C_State_Idle)
              {
                    LiDAR8_Trigger_tranmission();
              }
              else
              {
                    lidar8_request_flag = 1;
              }
              EXTI_ClearITPendingBit(EXTI_Line15);
        }
}

/* ============================================================================ */

uint16_t i2c2_firstbyte_rcv = 0;
void I2C2_IRQHandler(void)
{
        if (I2C_GetITStatus(I2C2, I2C_IT_TXIS))
        {
                switch(i2c2_service_state)
                {
                    case I2C_State_Lidar1GeneratedStartWriteandAddr:
                           i2c2_service_state = I2C_State_Lidar1SendingCMD;
                           I2C_SendData(I2C2, 0x8F);
                    break;


                    case I2C_State_Lidar2GeneratedStartWriteandAddr:
                           i2c2_service_state = I2C_State_Lidar2SendingCMD;
                           I2C_SendData(I2C2, 0x8F);
                    break;


                    case I2C_State_Lidar5GeneratedStartWriteandAddr:
                           i2c2_service_state = I2C_State_Lidar5SendingCMD;
                           I2C_SendData(I2C2, 0x8F);
                    break;


                    case I2C_State_Lidar6GeneratedStartWriteandAddr:
                           i2c2_service_state = I2C_State_Lidar6SendingCMD;
                           I2C_SendData(I2C2, 0x8F);
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C2, I2C_IT_TC))
        {
                switch (i2c2_service_state)
                {
                    case I2C_State_Lidar1SendingCMD:
                            i2c2_service_state = I2C_State_Lidar1GeneratedFirstStop;
                            I2C_TransferHandling(I2C2, LiDAR_1.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar2SendingCMD:
                            i2c2_service_state = I2C_State_Lidar2GeneratedFirstStop;
                            I2C_TransferHandling(I2C2, LiDAR_2.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar5SendingCMD:
                            i2c2_service_state = I2C_State_Lidar5GeneratedFirstStop;
                            I2C_TransferHandling(I2C2, LiDAR_5.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar6SendingCMD:
                            i2c2_service_state = I2C_State_Lidar6GeneratedFirstStop;
                            I2C_TransferHandling(I2C2, LiDAR_6.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C2, I2C_IT_RXNE))
        {
                switch (i2c2_service_state)
                {
                    case I2C_State_Lidar1GeneratedStartRead:
                            i2c2_service_state = I2C_State_Lidar1ReceivedFirstByte;
                            i2c2_firstbyte_rcv =  I2C_ReceiveData(I2C2);
                    break;
                    case I2C_State_Lidar1ReceivedFirstByte:                    
                            i2c2_service_state = I2C_State_Lidar1ReceivedSecondByte;
                            LiDAR_1.Data.distance = (i2c2_firstbyte_rcv<<8) | I2C_ReceiveData(I2C2);
                            LiDAR_1.DataReady = 1;
                    break;


                    case I2C_State_Lidar2GeneratedStartRead:                    
                            i2c2_service_state = I2C_State_Lidar2ReceivedFirstByte;
                            i2c2_firstbyte_rcv =  I2C_ReceiveData(I2C2);
                    break;
                    case I2C_State_Lidar2ReceivedFirstByte:                    
                            i2c2_service_state = I2C_State_Lidar2ReceivedSecondByte;
                            LiDAR_2.Data.distance = (i2c2_firstbyte_rcv<<8) | I2C_ReceiveData(I2C2);
                            LiDAR_2.DataReady = 1;
                    break;


                    case I2C_State_Lidar5GeneratedStartRead:                    
                            i2c2_service_state = I2C_State_Lidar5ReceivedFirstByte;
                            i2c2_firstbyte_rcv =  I2C_ReceiveData(I2C2);
                    break;
                    case I2C_State_Lidar5ReceivedFirstByte:                    
                            i2c2_service_state = I2C_State_Lidar5ReceivedSecondByte;
                            LiDAR_5.Data.distance = (i2c2_firstbyte_rcv<<8) | I2C_ReceiveData(I2C2);
                            LiDAR_5.DataReady = 1;
                    break;


                    case I2C_State_Lidar6GeneratedStartRead:                    
                            i2c2_service_state = I2C_State_Lidar6ReceivedFirstByte;
                            i2c2_firstbyte_rcv =  I2C_ReceiveData(I2C2);
                    break;
                    case I2C_State_Lidar6ReceivedFirstByte:                    
                            i2c2_service_state = I2C_State_Lidar6ReceivedSecondByte;
                            LiDAR_6.Data.distance = (i2c2_firstbyte_rcv<<8) | I2C_ReceiveData(I2C2);
                            LiDAR_6.DataReady = 1;
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C2, I2C_IT_STOPF))
        {
                I2C_ClearITPendingBit(I2C2, I2C_IT_STOPF);

                switch (i2c2_service_state)
                {
                    case I2C_State_Lidar1GeneratedFirstStop:
                            i2c2_service_state = I2C_State_Lidar1GeneratedStartRead;
                            I2C_TransferHandling(I2C2,  LiDAR_1.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);                      
                    break;
                    case I2C_State_Lidar1ReceivedSecondByte:
                            if(lidar2_request_flag == 1)
                            {
                                  lidar2_request_flag = 0;
                                  LiDAR2_Trigger_tranmission();
                            }
                            else if(lidar5_request_flag == 1)
                            {
                                  lidar5_request_flag = 0;
                                  LiDAR5_Trigger_tranmission();
                            }
                            else if(lidar6_request_flag == 1)
                            {
                                  lidar6_request_flag = 0;
                                  LiDAR6_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c2_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar2GeneratedFirstStop:
                            i2c2_service_state = I2C_State_Lidar2GeneratedStartRead;
                            I2C_TransferHandling(I2C2,  LiDAR_2.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar2ReceivedSecondByte:
                            if(lidar5_request_flag == 1)
                            {
                                  lidar5_request_flag = 0;
                                  LiDAR5_Trigger_tranmission();
                            }
                            else if(lidar6_request_flag == 1)
                            {
                                  lidar6_request_flag = 0;
                                  LiDAR6_Trigger_tranmission();
                            }
                            else if(lidar1_request_flag == 1)
                            {
                                  lidar1_request_flag = 0;
                                  LiDAR1_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c2_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar5GeneratedFirstStop:
                            i2c2_service_state = I2C_State_Lidar5GeneratedStartRead;
                            I2C_TransferHandling(I2C2,  LiDAR_5.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar5ReceivedSecondByte:
                            if(lidar6_request_flag == 1)
                            {
                                  lidar6_request_flag = 0;
                                  LiDAR6_Trigger_tranmission();
                            }
                            else if(lidar1_request_flag == 1)
                            {
                                  lidar1_request_flag = 0;
                                  LiDAR1_Trigger_tranmission();
                            }
                            else if(lidar2_request_flag == 1)
                            {
                                  lidar2_request_flag = 0;
                                  LiDAR2_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c2_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar6GeneratedFirstStop:
                            i2c2_service_state = I2C_State_Lidar6GeneratedStartRead;
                            I2C_TransferHandling(I2C2,  LiDAR_6.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar6ReceivedSecondByte:
                            if(lidar1_request_flag == 1)
                            {
                                  lidar1_request_flag = 0;
                                  LiDAR1_Trigger_tranmission();
                            }
                            else if(lidar2_request_flag == 1)
                            {
                                  lidar2_request_flag = 0;
                                  LiDAR2_Trigger_tranmission();
                            }
                            else if(lidar5_request_flag == 1)
                            {
                                  lidar5_request_flag = 0;
                                  LiDAR5_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c2_service_state = I2C_State_Idle;
                            }
                    break;


                    default:
                    break;
                }
        }
}

/* ============================================================================ */

uint16_t i2c1_firstbyte_rcv = 0;
void I2C1_IRQHandler(void)
{
        if (I2C_GetITStatus(I2C1, I2C_IT_TXIS))
        {
                switch(i2c1_service_state)
                {
                    case I2C_State_Lidar3GeneratedStartWriteandAddr:
                           i2c1_service_state = I2C_State_Lidar3SendingCMD;
                           I2C_SendData(I2C1, 0x8F);
                    break;


                    case I2C_State_Lidar4GeneratedStartWriteandAddr:
                           i2c1_service_state = I2C_State_Lidar4SendingCMD;
                           I2C_SendData(I2C1, 0x8F);
                    break;


                    case I2C_State_Lidar7GeneratedStartWriteandAddr:
                           i2c1_service_state = I2C_State_Lidar7SendingCMD;
                           I2C_SendData(I2C1, 0x8F);
                    break;


                    case I2C_State_Lidar8GeneratedStartWriteandAddr:
                           i2c1_service_state = I2C_State_Lidar8SendingCMD;
                           I2C_SendData(I2C1, 0x8F);
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C1, I2C_IT_TC))
        {
                switch (i2c1_service_state)
                {
                    case I2C_State_Lidar3SendingCMD:
                            i2c1_service_state = I2C_State_Lidar3GeneratedFirstStop;
                            I2C_TransferHandling(I2C1, LiDAR_3.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar4SendingCMD:
                            i2c1_service_state = I2C_State_Lidar4GeneratedFirstStop;
                            I2C_TransferHandling(I2C1, LiDAR_4.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar7SendingCMD:
                            i2c1_service_state = I2C_State_Lidar7GeneratedFirstStop;
                            I2C_TransferHandling(I2C1, LiDAR_7.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    case I2C_State_Lidar8SendingCMD:
                            i2c1_service_state = I2C_State_Lidar8GeneratedFirstStop;
                            I2C_TransferHandling(I2C1, LiDAR_8.Addr, 0, I2C_AutoEnd_Mode, I2C_Generate_Stop);
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C1, I2C_IT_RXNE))
        {
                switch (i2c1_service_state)
                {
                    case I2C_State_Lidar3GeneratedStartRead:
                            i2c1_service_state = I2C_State_Lidar3ReceivedFirstByte;
                            i2c1_firstbyte_rcv =  I2C_ReceiveData(I2C1);
                    break;
                    case I2C_State_Lidar3ReceivedFirstByte:                    
                            i2c1_service_state = I2C_State_Lidar3ReceivedSecondByte;
                            LiDAR_3.Data.distance = ((i2c1_firstbyte_rcv<<8) | I2C_ReceiveData(I2C1)) -6; // distance offset of lidar 3
                            LiDAR_3.DataReady = 1;
                    break;


                    case I2C_State_Lidar4GeneratedStartRead:                    
                            i2c1_service_state = I2C_State_Lidar4ReceivedFirstByte;
                            i2c1_firstbyte_rcv =  I2C_ReceiveData(I2C1);
                    break;
                    case I2C_State_Lidar4ReceivedFirstByte:                    
                            i2c1_service_state = I2C_State_Lidar4ReceivedSecondByte;
                            LiDAR_4.Data.distance = (i2c1_firstbyte_rcv<<8) | I2C_ReceiveData(I2C1);
                            LiDAR_4.DataReady = 1;
                    break;


                    case I2C_State_Lidar7GeneratedStartRead:                    
                            i2c1_service_state = I2C_State_Lidar7ReceivedFirstByte;
                            i2c1_firstbyte_rcv =  I2C_ReceiveData(I2C1);
                    break;
                    case I2C_State_Lidar7ReceivedFirstByte:                    
                            i2c1_service_state = I2C_State_Lidar7ReceivedSecondByte;
                            LiDAR_7.Data.distance = (i2c1_firstbyte_rcv<<8) | I2C_ReceiveData(I2C1);
                            LiDAR_7.DataReady = 1;
                    break;


                    case I2C_State_Lidar8GeneratedStartRead:                    
                            i2c1_service_state = I2C_State_Lidar8ReceivedFirstByte;
                            i2c1_firstbyte_rcv =  I2C_ReceiveData(I2C1);
                    break;
                    case I2C_State_Lidar8ReceivedFirstByte:                    
                            i2c1_service_state = I2C_State_Lidar8ReceivedSecondByte;
                            LiDAR_8.Data.distance = (i2c1_firstbyte_rcv<<8) | I2C_ReceiveData(I2C1);
                            LiDAR_8.DataReady = 1;
                    break;


                    default:
                    break;
                }
        }

        // ---------------------------------------------------------------------------------------------------------------
        if (I2C_GetITStatus(I2C1, I2C_IT_STOPF))
        {
                I2C_ClearITPendingBit(I2C1, I2C_IT_STOPF);

                switch (i2c1_service_state)
                {
                    case I2C_State_Lidar3GeneratedFirstStop:
                            i2c1_service_state = I2C_State_Lidar3GeneratedStartRead;
                            I2C_TransferHandling(I2C1,  LiDAR_3.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);                      
                    break;
                    case I2C_State_Lidar3ReceivedSecondByte:
                            if(lidar4_request_flag == 1)
                            {
                                  lidar4_request_flag = 0;
                                  LiDAR4_Trigger_tranmission();
                            }
                            else if(lidar7_request_flag == 1)
                            {
                                  lidar7_request_flag = 0;
                                  LiDAR7_Trigger_tranmission();
                            }
                            else if(lidar8_request_flag == 1)
                            {
                                  lidar8_request_flag = 0;
                                  LiDAR8_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c1_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar4GeneratedFirstStop:
                            i2c1_service_state = I2C_State_Lidar4GeneratedStartRead;
                            I2C_TransferHandling(I2C1,  LiDAR_4.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar4ReceivedSecondByte:
                            if(lidar7_request_flag == 1)
                            {
                                  lidar7_request_flag = 0;
                                  LiDAR7_Trigger_tranmission();
                            }
                            else if(lidar8_request_flag == 1)
                            {
                                  lidar8_request_flag = 0;
                                  LiDAR8_Trigger_tranmission();
                            }
                            else if(lidar3_request_flag == 1)
                            {
                                  lidar3_request_flag = 0;
                                  LiDAR3_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c1_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar7GeneratedFirstStop:
                            i2c1_service_state = I2C_State_Lidar7GeneratedStartRead;
                            I2C_TransferHandling(I2C1,  LiDAR_7.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar7ReceivedSecondByte:
                            if(lidar8_request_flag == 1)
                            {
                                  lidar8_request_flag = 0;
                                  LiDAR8_Trigger_tranmission();
                            }
                            else if(lidar3_request_flag == 1)
                            {
                                  lidar3_request_flag = 0;
                                  LiDAR3_Trigger_tranmission();
                            }
                            else if(lidar4_request_flag == 1)
                            {
                                  lidar4_request_flag = 0;
                                  LiDAR4_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c1_service_state = I2C_State_Idle;
                            }
                    break;


                    case I2C_State_Lidar8GeneratedFirstStop:
                            i2c1_service_state = I2C_State_Lidar8GeneratedStartRead;
                            I2C_TransferHandling(I2C1,  LiDAR_8.Addr, 2, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);
                    break;
                    case I2C_State_Lidar8ReceivedSecondByte:
                            if(lidar3_request_flag == 1)
                            {
                                  lidar3_request_flag = 0;
                                  LiDAR3_Trigger_tranmission();
                            }
                            else if(lidar4_request_flag == 1)
                            {
                                  lidar4_request_flag = 0;
                                  LiDAR4_Trigger_tranmission();
                            }
                            else if(lidar7_request_flag == 1)
                            {
                                  lidar7_request_flag = 0;
                                  LiDAR7_Trigger_tranmission();
                            }
                            else
                            {
                                  i2c1_service_state = I2C_State_Idle;
                            }
                    break;


                    default:
                    break;
                }
        }
}