#include "main.h"
#include "ecm1418.h"

/* Encoder data =================================================================
** ID 		: LQ_ECM14180305_SDZ512
** voltage 	: 3.3 ~ 5V
** size		: diameter 14mm / height 18mm / axis 3mm
** pulse counter : 512
** weight	: 11.22
** pin 		: PC3 - encoder STEP
		  PB0 - encoder Z
		  PB1 - encoder DIR
============================================================================== */

ECM1418_t ECM1418 = {
	.step = 0,
	.z = 0,
	.dir = 0	
	// for hardware, 1 for clockwise, 0 for counter-clockwise
	// here for meeting the need of ROS LaserScan.msg, we set -1 for clockwise and 1 for counter-clockwise for software.
};

uint16_t RoundCounter = 0;

/* ============================================================================ */

void ECM1418_Configuration(void)
{
	// RCC ---------------------------------------------------------------------------------------
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	// GPIO -------------------------------------------------------------------------------------
  	GPIO_InitTypeDef GPIO_InitStructure = {
	            .GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1,
	            .GPIO_Mode = GPIO_Mode_IN,
	            .GPIO_PuPd = GPIO_PuPd_DOWN
  	};
  	GPIO_Init(GPIOB, &GPIO_InitStructure);
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  	GPIO_Init(GPIOC, &GPIO_InitStructure);

  	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, GPIO_PinSource0);
  	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, GPIO_PinSource1);
  	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, GPIO_PinSource3);

  	// EXTI --------------------------------------------------------------------------------------
  	EXTI_InitTypeDef EXTI_InitStructure = {
  		.EXTI_Line = EXTI_Line0 | EXTI_Line3,		// encoder Z STEP
  		.EXTI_Mode = EXTI_Mode_Interrupt,
  		.EXTI_Trigger = EXTI_Trigger_Rising,
  		.EXTI_LineCmd = ENABLE
  	};
  	EXTI_Init(&EXTI_InitStructure);
  	EXTI_InitStructure.EXTI_Line = EXTI_Line1;		// encoder DIR
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
  	EXTI_Init(&EXTI_InitStructure);

  	// NVIC --------------------------------------------------------------------------------------
	NVIC_InitTypeDef NVIC_InitStructure = {
		.NVIC_IRQChannel = EXTI0_1_IRQn,		// encoder Z DIR
		.NVIC_IRQChannelPriority = 0x02,
		.NVIC_IRQChannelCmd = ENABLE
	};
	NVIC_Init(&NVIC_InitStructure);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_3_IRQn;	// encoder STEP
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x02;
	NVIC_Init(&NVIC_InitStructure);
}

/* ============================================================================ */

void EXTI0_1_IRQHandler(void)
{
	if (EXTI_GetFlagStatus(EXTI_Line0))  	// encoder Z
	{
		ECM1418.step = 0;
		EXTI_ClearITPendingBit(EXTI_Line0);
		# if PrintSerialData
			sprintf(buffer_uart, "zero \r\n");
			usart3_dma_send_text(buffer_uart); 
		# endif
	}
	if (EXTI_GetFlagStatus(EXTI_Line1))	// encoder DIR
	{					
		// 1 for clockwise, 0 for counter-clockwise
		ECM1418.dir = (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1) ? -1 : 1); 
		EXTI_ClearITPendingBit(EXTI_Line1);
	}
}

/* ============================================================================ */

static uint32_t TimeAnchor_1 = 0;
static uint32_t TimeAnchor_2 = 0;
uint32_t TimeInterval[5] = {2462, 2462, 2462, 2462, 2462};
// 2462 is the avg time interval, use as the initialization value here
uint32_t AvgTimeInterval = 2462;
void EXTI2_3_IRQHandler(void)	// encoder STEP
{	
	ECM1418.step += ECM1418.dir;
	if (ECM1418.step < 0) ECM1418.step += 512;

	EXTI_ClearITPendingBit(EXTI_Line3);
	TimeAnchor_1 = TimeAnchor_2;
	TimeAnchor_2 = TIM_GetCounter(TIM2);
	if (TimeAnchor_2 < TimeAnchor_1) TimeInterval[ECM1418.step%5] = TimeAnchor_2 + 2000000 - TimeAnchor_1;
	else TimeInterval[ECM1418.step%5] = TimeAnchor_2 - TimeAnchor_1;
	AvgTimeInterval = (TimeInterval[0] + TimeInterval[1] + TimeInterval[2] + TimeInterval[3] + TimeInterval[4]) * 0.2;

	// sprintf(buffer_uart_ECM1418, "step : %d, TimeStamp : %d, AvgTime : %d\n\r", ECM1418.step, TimeAnchor_2, AvgTimeInterval);
	// TimeStampPrintFlag = 1;
}

/* ============================================================================ */

float GetEncoderAngle(uint32_t TimeStamp)
{
	float step = (float)ECM1418.step;
	float DeltaStep = 0.0;
	
	if ((TimeStamp == TimeAnchor_2) || (TimeAnchor_1 == TimeAnchor_2))
	{
		return (step) * (float)0.703125;	 // = 360 / 512
	}
	if (TimeStamp < TimeAnchor_2)
	{
		TimeStamp += 2000000;
	}
	if ((TimeStamp - TimeAnchor_2) > AvgTimeInterval* 1.2)
	{
		DeltaStep = (AvgTimeInterval * 0.5 / AvgTimeInterval)*ECM1418.dir;
	}
	else
	{
		DeltaStep = ((float)((TimeStamp - TimeAnchor_2) / AvgTimeInterval))*ECM1418.dir;
	}
	// (TimeAnchor_2 - TimeAnchor_1) : (TimeStamp - TimeAnchor_2) = 1 : step
	return (step + DeltaStep) * (float)0.703125;
}

/* ============================================================================ */

void TIM2_Configuration(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure = {
	            .NVIC_IRQChannel = TIM2_IRQn,
	            .NVIC_IRQChannelPriority = 0x01,
	            .NVIC_IRQChannelCmd = ENABLE
    	};
    	NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_InitStructure = {
		.TIM_Prescaler = 48 -1, 	// 48
		.TIM_CounterMode = TIM_CounterMode_Up,
		.TIM_Period = 2000000 -1,
		.TIM_ClockDivision = TIM_CKD_DIV1,
		// .TIM_RepetitionCounter only for TMI1
	};
	TIM_TimeBaseInit(TIM2, &TIM_InitStructure);

	TIM_OCInitTypeDef TIM_OCInitStructure = {
		.TIM_OCMode = TIM_OCMode_Timing,
		.TIM_OutputState = TIM_OutputState_Enable,
		// .TIM_OutputNState only for TIM1
		.TIM_Pulse = 2000000 -1,	// 1000
		.TIM_OCPolarity = TIM_OCPolarity_High,
		// .TIM_OCNPolarity only for TIM1
		// .TIM_OCIdleState only for TIM1
		// .TIM_OCNIdleState only for TIM1
	};
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);
	// TIM_CCPreloadControl();
	TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);
    	TIM_Cmd(TIM2, ENABLE);
}

/* ============================================================================ */

void TIM2_IRQHandler(void)
{	
	//if (TIM_GetITStatus(TIM2, TIM_IT_CC1))
	//{
		// sprintf(buffer_uart, "TimeStamp. RoundCounter = %d\r\n", RoundCounter);
		// usart3_dma_send_text(buffer_uart); 
		GPIOC -> ODR ^= GPIO_Pin_6;
		// RoundCounter ++;
	//}
	# if SendPointNum0xAE
		memcpy(buffer_uart_PointNum_0xAE+3, &PointNum, 2);
		buffer_uart_PointNum_0xAE[5] = Get_SendPointNum0xAEChkSum();
		usart3_dma_send_PointNum_0xAE(buffer_uart_PointNum_0xAE);
		PointNum = 0;
	# endif

	TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
}