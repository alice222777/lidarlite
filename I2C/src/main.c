# include "main.h"

// Settings -------------------------------------------------------------------------------------------------------

uint8_t StartByte1 = 0xCA;
uint8_t StartByte2 = 0x35;

# if Package0xAA
	uint8_t buffer_uart_Lidar[14];
# endif
# if Package0xAB
	uint8_t buffer_uart_imu[16];
# endif
# if Package0xAC
	uint8_t buffer_uart_Lidar2[11];
# endif
# if Package0xAD
	uint8_t buffer_uart_Lidar_0xAD[15];
# endif
# if SendPointNum0xAE
	uint16_t PointNum = 0;
	uint8_t buffer_uart_PointNum_0xAE[6];
#endif
int buffer_uart[100];


int main(void)
{	
	# if Package0xAA
		buffer_uart_Lidar[0] = StartByte1;
		buffer_uart_Lidar[1] = StartByte2;
		buffer_uart_Lidar[2] = 0xAA;
	# endif
	# if Package0xAB
		buffer_uart_imu[0] = StartByte1;
		buffer_uart_imu[1] = StartByte2;
		buffer_uart_imu[2] = 0xAB;
	# endif
	# if Package0xAC
		buffer_uart_Lidar2[0] = StartByte1;
		buffer_uart_Lidar2[1] = StartByte2;
		buffer_uart_Lidar2[2] = 0xAC;
	# endif
	# if Package0xAD
		buffer_uart_Lidar_0xAD[0] = StartByte1;
		buffer_uart_Lidar_0xAD[1] = StartByte2;
		buffer_uart_Lidar_0xAD[2] = 0xAD;
	# endif
	# if SendPointNum0xAE
		buffer_uart_PointNum_0xAE[0] = StartByte1;
		buffer_uart_PointNum_0xAE[1] = StartByte2;
		buffer_uart_PointNum_0xAE[2] = 0xAE;
	#endif


	// Configurations --------------------------------------------------------------------------------------
	LED_Configuration();
	USART3_Initialization();
	Delay_1us(5000);
	USART3_dma_init();
	LiDAR_PwrEn_Configuration();
	GPIO_SetBits(GPIOC, GPIO_Pin_6);	// LED red for debug
	I2C2_Configuration();
	GPIO_SetBits(GPIOC, GPIO_Pin_7);	// LED blue for debug
	I2C1_Configuration(); 
	// USART1_Initialization();

	// Change the address of slaves -------------------------------------------------------------------
	// Delay_1us(10000);
	// LiDAR_ChangeMultiAddress(I2C2);	// Change the address of LiDAR 1 and 2
	// Delay_1us(10000);
	// LiDAR_ChangeMultiAddress(I2C1);	// Change the address of LiDAR 3 and 4
	// GPIO_SetBits(GPIOC, GPIO_Pin_9);	// LED orange for debug

	LidarAddr_Configuration();
	GPIO_ResetBits(GPIOC, GPIO_Pin_6);	// LED orange for debug

	// LiDAR initialization ----------------------------------------------------------------------------------
	Delay_1us(500000);
	LiDAR_begin(I2C2, LiDAR_1.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C2, LiDAR_2.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C1, LiDAR_3.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C1, LiDAR_4.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C2, LiDAR_5.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C2, LiDAR_6.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C1, LiDAR_7.Addr, CONFIG_HighSpeed);
	LiDAR_begin(I2C1, LiDAR_8.Addr, CONFIG_HighSpeed);
	GPIO_ResetBits(GPIOC, GPIO_Pin_7);	// LED orange for debug
	Delay_1us(500000);
	LiDAR_beginContinuous(I2C2, LiDAR_1.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C2, LiDAR_2.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C1, LiDAR_3.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C1, LiDAR_4.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C2, LiDAR_5.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C2, LiDAR_6.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C1, LiDAR_7.Addr, 0xFF, 0x02);
	LiDAR_beginContinuous(I2C1, LiDAR_8.Addr, 0xFF, 0x02);

	ECM1418_Configuration();

	// interrupt -------------------------------------------------------------------------------------------------
	I2C_ITConfig(I2C2, I2C_IT_TCI | I2C_IT_STOPI | I2C_IT_RXI | I2C_IT_TXI , ENABLE);
	I2C_ITConfig(I2C1, I2C_IT_TCI | I2C_IT_STOPI | I2C_IT_RXI | I2C_IT_TXI , ENABLE);
	GPIO_ResetBits(GPIOC, GPIO_Pin_9);
	Delay_1us(500000);
	// mode pin config must be the last one, or the IRQ will start working when the initialization is not finished yet.
	LiDAR_Mode_Configuration();	
	TIM2_Configuration();
	GPIO_SetBits(GPIOC, GPIO_Pin_9);	// LED red for debug
	Delay_1us(50000);
	GPIO_ResetBits(GPIOC, GPIO_Pin_9);	// LED orange for debug

	// while(1) GPIOC -> ODR ^= GPIO_Pin_7;		// BLUE
	// int alignment = 0;
	
	// SPI test ---------------------------------------------------------------------------------------------------
	// spi1_dma_enable_receive_fast(spi1_buffer);
	// spi1_dma_transmit_fast(spi1_RequestAddr);
	// spi2_GetIMUData(spi2_RequestAddr);
	// GPIOB->ODR ^= GPIO_Pin_12;

	while(1)
	{
		// distance single mode ---------------------------------------------------------------------
		// LiDAR_1.Data.distance = LiDAR_distance(I2C2, LiDAR_1.Addr);
		// LiDAR_2.Data.distance = LiDAR_distance(I2C2, LiDAR_2.Addr);
		// LiDAR_3.Data.distance = LiDAR_distance(I2C1, LiDAR_3.Addr);
		// LiDAR_4.Data.distance = LiDAR_distance(I2C1, LiDAR_4.Addr);

		// distance continuous mode --------------------------------------------------------------
		// LiDAR_1_distanceContinuous();
		// LiDAR_2_distanceContinuous();
		// LiDAR_3_distanceContinuous();
		// LiDAR_4_distanceContinuous();

		// LED ----------------------------------------------------------------------------------------------
		// GPIOC -> ODR ^= GPIO_Pin_6;	// RED	
		// GPIOC -> ODR ^= GPIO_Pin_7;	// BLUE
		// GPIOC -> ODR ^= GPIO_Pin_9;	// GREEN
		// Delay_1us (50000);

		// test for zero point -----------------------------------------------------------------------------------------------
		// sprintf(buffer_uart, "%3.4f\r\n", (float)ECM1418.step*0.703125);
		// usart3_dma_send_text(buffer_uart); 

		# if PrintSerialData
		// if ( TimeStampPrintFlag == 1)
		// {
		// 	usart3_dma_send_text(buffer_uart_ECM1418);
		// 	TimeStampPrintFlag = 0;
		// }
		if (LiDAR_1.DataReady && (LiDAR_1.Data.distance >10) && (LiDAR_1.Data.distance <4000))
		{
    		GPIOC->ODR ^= GPIO_Pin_6;
    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
			sprintf(buffer_uart, "(1) %3d,%7d,%3.4f\r\n", LiDAR_1.Data.distance, LiDAR_1.Data.TimeStamp, LiDAR_1.Data.angle);
			usart3_dma_send_text(buffer_uart);
			LiDAR_1.DataReady = 0;
		}
		if (LiDAR_2.DataReady && (LiDAR_2.Data.distance >10) && (LiDAR_2.Data.distance <4000))
		{
    		GPIOC->ODR ^= GPIO_Pin_7;
    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
			sprintf(buffer_uart, "(2) %3d,%7d,%3.4f\r\n", LiDAR_2.Data.distance, LiDAR_2.Data.TimeStamp, LiDAR_2.Data.angle);
			usart3_dma_send_text(buffer_uart);
			LiDAR_2.DataReady = 0;
		}
		if (LiDAR_3.DataReady && (LiDAR_3.Data.distance >10) && (LiDAR_3.Data.distance <4000))
		{
    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
			sprintf(buffer_uart, "(3) %3d,%7d,%3.4f\r\n", LiDAR_3.Data.distance, LiDAR_3.Data.TimeStamp, LiDAR_3.Data.angle);
			usart3_dma_send_text(buffer_uart);
			LiDAR_3.DataReady = 0;
		}
		if (LiDAR_4.DataReady && (LiDAR_4.Data.distance >10) && (LiDAR_4.Data.distance <4000))
		{
			GPIOC->ODR ^= GPIO_Pin_9;
    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
			sprintf(buffer_uart, "(4) %3d,%7d,%3.4f\r\n", LiDAR_4.Data.distance, LiDAR_4.Data.TimeStamp, LiDAR_4.Data.angle);
			usart3_dma_send_text(buffer_uart);
			LiDAR_4.DataReady = 0;
		}
		# endif

		# if Package0xAA // ---------------------------------------------------------------------------------------------
			if (LiDAR_1.DataReady && (LiDAR_1.Data.distance >10) && (LiDAR_1.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		memcpy(buffer_uart_Lidar+3, &LiDAR_1.Data, 10);
			 	buffer_uart_Lidar[13] = Get_BufferDataChkSum(buffer_uart_Lidar);
	    		usart3_dma_send_LidarData(buffer_uart_Lidar); 
				LiDAR_1.DataReady = 0;
			}
			if (LiDAR_2.DataReady && (LiDAR_2.Data.distance >10) && (LiDAR_2.Data.distance <4000))
			{
	  			while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	  		  	memcpy(buffer_uart_Lidar+3, &LiDAR_2.Data, 10);
			 	buffer_uart_Lidar[13] = Get_BufferDataChkSum(buffer_uart_Lidar);
				usart3_dma_send_LidarData(buffer_uart_Lidar); 
				LiDAR_2.DataReady = 0;
			}
			if (LiDAR_3.DataReady && (LiDAR_3.Data.distance >10) && (LiDAR_3.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		memcpy(buffer_uart_Lidar+3, &LiDAR_3.Data, 10);
			 	buffer_uart_Lidar[13] = Get_BufferDataChkSum(buffer_uart_Lidar);
				usart3_dma_send_LidarData(buffer_uart_Lidar); 
				LiDAR_3.DataReady = 0;
			}
			if (LiDAR_4.DataReady && (LiDAR_4.Data.distance >10) && (LiDAR_4.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_9;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		memcpy(buffer_uart_Lidar+3, &LiDAR_4.Data, 10);
			 	buffer_uart_Lidar[13] = Get_BufferDataChkSum(buffer_uart_Lidar);
				usart3_dma_send_LidarData(buffer_uart_Lidar); 
				LiDAR_4.DataReady = 0;
			}
		# endif


		# if Package0xAB
			IMU.ACCEL_X =  SPI2_Get2bytes(ACCEL_X);
			IMU.ACCEL_Y =  SPI2_Get2bytes(ACCEL_Y);
			IMU.ACCEL_Z =  SPI2_Get2bytes(ACCEL_Z);
			IMU.GYRO_X =  SPI2_Get2bytes(GYRO_X);
			IMU.GYRO_Y =  SPI2_Get2bytes(GYRO_Y);
			IMU.GYRO_Z =  SPI2_Get2bytes(GYRO_Z);
			memcpy(buffer_uart_imu+3, &IMU, 12);
			buffer_uart_imu[15] = Get_ImuDataChkSum();
			usart3_dma_send_ImuData(buffer_uart_imu); 
			sprintf(buffer_uart, "%X, %X, %X, %X, %X, %X, %X, %d, %d\r\n", IMU.ACCEL_X, IMU.ACCEL_Y, IMU.ACCEL_Z, IMU.GYRO_X, IMU.GYRO_Y,IMU.GYRO_Z, buffer_uart_imu[15], sizeof(buffer_uart_imu), sizeof(IMU_t));
			usart3_dma_send_text(buffer_uart); 	
		# endif


		# if Package0xAC // ---------------------------------------------------------------------------------------------
			if (LiDAR_1.DataReady && (LiDAR_1.Data.distance >10) && (LiDAR_1.Data.distance <4000))
			{
	    		GPIOC->ODR ^= GPIO_Pin_6;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x01;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_1.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_1.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_1.DataReady = 0;
				# if SendPointNum0xAE
					PointNum++;
				# endif
			}
			if (LiDAR_2.DataReady && (LiDAR_2.Data.distance >10) && (LiDAR_2.Data.distance <4000))
			{
	    		GPIOC->ODR ^= GPIO_Pin_6;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x02;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_2.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_2.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_2.DataReady = 0;
				# if SendPointNum0xAE
					PointNum++;
				# endif
			}
			if (LiDAR_3.DataReady && (LiDAR_3.Data.distance >10) && (LiDAR_3.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_7;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x03;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_3.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_3.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_3.DataReady = 0;
				# if SendPointNum0xAE
					PointNum++;
				# endif
			}
			if (LiDAR_4.DataReady && (LiDAR_4.Data.distance >10) && (LiDAR_4.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_7;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x04;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_4.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_4.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_4.DataReady = 0;
				# if SendPointNum0xAE
					PointNum++;
				# endif
			}
			if (LiDAR_5.DataReady && (LiDAR_5.Data.distance >10) && (LiDAR_5.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x05;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_5.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_5.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_5.DataReady = 0;
			}
			if (LiDAR_6.DataReady && (LiDAR_6.Data.distance >10) && (LiDAR_6.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x06;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_6.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_6.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_6.DataReady = 0;
			}
			if (LiDAR_7.DataReady && (LiDAR_7.Data.distance >10) && (LiDAR_7.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_9;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x07;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_7.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_7.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_7.DataReady = 0;
			}
			if (LiDAR_8.DataReady && (LiDAR_8.Data.distance >10) && (LiDAR_8.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_9;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar2[3] = 0x08;
	    		memcpy(buffer_uart_Lidar2+4, &LiDAR_8.Data.angle, 4);
	    		memcpy(buffer_uart_Lidar2+8, &LiDAR_8.Data.distance, 2);
			 	buffer_uart_Lidar2[10] = Get_LidarData2ChkSum();
	    		usart3_dma_send_LidarData2(buffer_uart_Lidar2); 
				LiDAR_8.DataReady = 0;
			}
		# endif
		 

		# if Package0xAD // ---------------------------------------------------------------------------------------------
			if (LiDAR_1.DataReady && (LiDAR_1.Data.distance >10) && (LiDAR_1.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar_0xAD[3] = 0x01;
	    		memcpy(buffer_uart_Lidar_0xAD+4, &LiDAR_1.Data, 10);
			 	buffer_uart_Lidar_0xAD[14] = Get_LidarData0xADChkSum();
	    		usart3_dma_send_LidarData_0xAD(buffer_uart_Lidar_0xAD); 
				LiDAR_1.DataReady = 0;
			}
			if (LiDAR_2.DataReady && (LiDAR_2.Data.distance >10) && (LiDAR_2.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar_0xAD[3] = 0x02;
	    		memcpy(buffer_uart_Lidar_0xAD+4, &LiDAR_2.Data, 10);
			 	buffer_uart_Lidar_0xAD[14] = Get_LidarData0xADChkSum();
	    		usart3_dma_send_LidarData_0xAD(buffer_uart_Lidar_0xAD); 
				LiDAR_2.DataReady = 0;
			}
			if (LiDAR_3.DataReady && (LiDAR_3.Data.distance >10) && (LiDAR_3.Data.distance <4000))
			{
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar_0xAD[3] = 0x03;
	    		memcpy(buffer_uart_Lidar_0xAD+4, &LiDAR_3.Data, 10);
			 	buffer_uart_Lidar_0xAD[14] = Get_LidarData0xADChkSum();
	    		usart3_dma_send_LidarData_0xAD(buffer_uart_Lidar_0xAD); 
				LiDAR_3.DataReady = 0;
			}
			if (LiDAR_4.DataReady && (LiDAR_4.Data.distance >10) && (LiDAR_4.Data.distance <4000))
			{
				GPIOC->ODR ^= GPIO_Pin_9;
	    		while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
	    		buffer_uart_Lidar_0xAD[3] = 0x04;
	    		memcpy(buffer_uart_Lidar_0xAD+4, &LiDAR_4.Data, 10);
			 	buffer_uart_Lidar_0xAD[14] = Get_LidarData0xADChkSum();
	    		usart3_dma_send_LidarData_0xAD(buffer_uart_Lidar_0xAD); 
				LiDAR_4.DataReady = 0;
			}
		# endif

		// SPI test ---------------------------------------------------------------------------------------------------
    		 // if(dma_spi1_complete_flag==1)
		// {
		// 	dma_spi1_complete_flag=0;
		// 	sprintf(buffer_uart, "%X,%X,\r\n", uart_from_spi1_buffer[0], uart_from_spi1_buffer[1]);
		// 	usart3_dma_send_text(buffer_uart); 
		// };
	}
}

/* ============================================================================ */

void Delay_1us(uint32_t nCnt_1us)
{
  volatile uint32_t nCnt;
  nCnt_1us = nCnt_1us/6;
  for (; nCnt_1us != 0; nCnt_1us--)
    for (nCnt = 16; nCnt != 0; nCnt--);
}

/* ============================================================================ */

void LED_Configuration(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

  GPIO_InitTypeDef GPIO_InitStructure = {
            .GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_9,
            .GPIO_Mode = GPIO_Mode_OUT,
            .GPIO_Speed = GPIO_Speed_50MHz,
            .GPIO_OType = GPIO_OType_PP,
            .GPIO_PuPd = GPIO_PuPd_NOPULL
  };
  GPIO_Init(GPIOC, &GPIO_InitStructure);

}

/* ============================================================================ */

// void FreeGPIO_Configuration(void)
// {
// 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
// 	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE);

//   	GPIO_InitTypeDef GPIO_InitStructure = {
// 	            .GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_8 | GPIO_Pin_9,
// 	            .GPIO_Mode = GPIO_Mode_OUT,
// 	            .GPIO_Speed = GPIO_Speed_50MHz,
// 	            .GPIO_OType = GPIO_OType_PP,
// 	            .GPIO_PuPd = GPIO_PuPd_NOPULL
// 	  };
//   	GPIO_Init(GPIOB, &GPIO_InitStructure);
//   	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
//   	GPIO_Init(GPIOD, &GPIO_InitStructure);
// }

/* ============================================================================ */

# if Package0xAB
uint8_t Get_ImuDataChkSum(void)
{
        uint8_t ChkSum = 0;
        unsigned int counter = 0;

        for (counter = 0; counter < sizeof(buffer_uart_imu) -1; counter++)
        {
              ChkSum += buffer_uart_imu[counter];
        };

        return ~ChkSum;
}
# endif

/* ============================================================================ */

# if Package0xAA
uint8_t Get_LidarDataChkSum(void)
{
        uint8_t ChkSum = 0;
        unsigned int counter = 0;

        for (counter = 0; counter < sizeof(buffer_uart_Lidar) -1; counter++)
        {
              ChkSum += buffer_uart_Lidar[counter];
        };

        return ~ChkSum;
}
#endif

/* ============================================================================ */

# if Package0xAC
uint8_t Get_LidarData2ChkSum(void)
{
        uint8_t ChkSum = 0;
        unsigned int counter = 0;

        for (counter = 0; counter < sizeof(buffer_uart_Lidar2) -1; counter++)
        {
              ChkSum += buffer_uart_Lidar2[counter];
        };

        return ~ChkSum;
}
# endif

/* ============================================================================ */

# if Package0xAD
uint8_t Get_LidarData0xADChkSum(void)
{
        uint8_t ChkSum = 0;
        unsigned int counter = 0;

        for (counter = 0; counter < sizeof(buffer_uart_Lidar_0xAD) -1; counter++)
        {
              ChkSum += buffer_uart_Lidar_0xAD[counter];
        };

        return ~ChkSum;
}
# endif

/* ============================================================================ */

# if SendPointNum0xAE
uint8_t Get_SendPointNum0xAEChkSum(void)
{
        uint8_t ChkSum = 0;
        unsigned int counter = 0;

        for (counter = 0; counter < sizeof(buffer_uart_PointNum_0xAE) -1; counter++)
        {
              ChkSum += buffer_uart_PointNum_0xAE[counter];
        };

        return ~ChkSum;
}
# endif