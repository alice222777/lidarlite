#include "spi.h"

/* SPI Settings /////////////////////////////////////////////////////////////////////////////////////////

SPI1
--- SCK : PB3
--- MISO : PB4
--- MOSI : PB5
--- nCS : PC7

SPI2
--- SCK : PB13
--- MISO : PB14
--- MOSI : PB15
--- nCS : PB12

///////////////////////////////////////////////////////////////////////////////////////////////////////*/

IMU_t IMU = {
        .ACCEL_X = 0,
        .ACCEL_Y = 0,
        .ACCEL_Z = 0,
        .GYRO_X = 0,
        .GYRO_Y = 0,
        .GYRO_Z = 0,
        // .TEMP = 0
};

// MPU6500 settings --------------------------------------------------------------------------------------------------
uint8_t WHOAMI[]= {0xF5, 0xF5}; //ACCEL_XOUT_H and L
uint8_t ACCEL_X[]= {0xBB, 0xBC}; //ACCEL_XOUT_H and L
uint8_t ACCEL_Y[]= {0xBD, 0xBE}; //ACCEL_YOUT_H and L
uint8_t ACCEL_Z[]= {0xBF, 0xC0,}; //ACCEL_ZOUT_H and L
uint8_t GYRO_X[]= {0xC3, 0xC4}; //GYRO_X_OUT_H and L
uint8_t GYRO_Y[]= {0xC5, 0xC6}; //GYRO_Y_OUT_H and L
uint8_t GYRO_Z[]= {0xC7, 0xC8}; //GYRO_Z_OUT_H and L
uint8_t TEMP[]= {0xC1, 0xC2}; //TEMP_H and L
// ---------------------------------------------------------------------------------------------------------------------------

uint8_t SPI_xfer(SPI_TypeDef *SPIx, uint8_t  WriteByte)
{
	uint8_t rxdata;

	SPI_SendData8(SPIx, (uint16_t) WriteByte);
	while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET);
	//while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET);
	rxdata = SPI_ReceiveData8(SPIx);
             GPIOC -> ODR ^= GPIO_Pin_9;

	return rxdata;
}

/* ============================================================================ */

uint16_t SPI2_Get2bytes(uint8_t *AddrArray)
{
            uint16_t Data = 0;

            GPIO_ResetBits(GPIOB, GPIO_Pin_12);             // nCS
            SPI_xfer(SPI2, AddrArray[0]);
            Data = SPI_xfer(SPI2, AddrArray[1]);
            Data = (Data<<8) | SPI_xfer(SPI2, 0x00);
            Delay_1us(150);
            GPIO_SetBits(GPIOB, GPIO_Pin_12);

            return Data;
}

/* ============================================================================ */

void SPI1_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_InitTypeDef SPI_InitStruct;

            RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
            RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
            RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
            RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	/* CSN PB12 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);

	// GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;      // nCS
	// GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	// GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_0);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_0);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_0);

	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // 雙線全雙工
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master; // 主模式
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; // 數據大小8位
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_High; // 時鐘極性，空閒時為低
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge; // 第1個邊沿有效，上升沿為采樣時刻
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft; // NSS信號由軟件產生
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; // 8分頻，9MHz
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB; // 高位在前
	SPI_InitStruct.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStruct);

  	SPI_RxFIFOThresholdConfig(SPI1, SPI_RxFIFOThreshold_QF);
	SPI_Cmd(SPI1, ENABLE);

            /* NVIC Initialization */
            NVIC_InitTypeDef NVIC_InitStruct = {
                    .NVIC_IRQChannel = DMA1_Channel2_3_IRQn ,
                    .NVIC_IRQChannelPriority = 0,
                    .NVIC_IRQChannelCmd = ENABLE
            };
            NVIC_Init(&NVIC_InitStruct);
            
            DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, DISABLE);
            /* Enable DMA RX finish */
            DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);

            GPIOC->ODR ^= GPIO_Pin_7;
}

/* ============================================================================ */

void SPI2_Configuration(void)
{
            RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
            RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

            GPIO_InitTypeDef GPIO_InitStruct = {
                    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15,
                    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF,
                    GPIO_InitStruct.GPIO_OType = GPIO_OType_OD,
                    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL,
                    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz
            };
            GPIO_Init(GPIOB, &GPIO_InitStruct);

            GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;
            GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
            GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
            GPIO_Init(GPIOB, &GPIO_InitStruct);

            GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_0);
            GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_0);
            GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_0);

            SPI_InitTypeDef SPI_InitStruct = {
                    .SPI_Direction = SPI_Direction_2Lines_FullDuplex, // 雙線全雙工
                    .SPI_Mode = SPI_Mode_Master, // 主模式
                    .SPI_DataSize = SPI_DataSize_8b, // 數據大小8位
                    .SPI_CPOL = SPI_CPOL_High, // 時鐘極性，空閒時為低
                    .SPI_CPHA = SPI_CPHA_2Edge, // 第1個邊沿有效，上升沿為采樣時刻
                    .SPI_NSS = SPI_NSS_Soft, // NSS信號由軟件產生
                    .SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256, // 8分頻，9MHz
                    .SPI_FirstBit = SPI_FirstBit_MSB, // 高位在前
                    .SPI_CRCPolynomial = 7
            };
            SPI_Init(SPI2, &SPI_InitStruct);

            SPI_RxFIFOThresholdConfig(SPI2, SPI_RxFIFOThreshold_QF);
            SPI_Cmd(SPI2, ENABLE);

            /* NVIC Initialization */
            // NVIC_InitTypeDef NVIC_InitStruct = {
            //         .NVIC_IRQChannel = DMA1_Channel4_5_IRQn ,
            //         .NVIC_IRQChannelPriority = 0x01,
            //         .NVIC_IRQChannelCmd = ENABLE
            // };
            // NVIC_Init(&NVIC_InitStruct);
                    
            // DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, DISABLE);
            // /* Enable DMA RX finish */
            // DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);

            // GPIOB->ODR ^= GPIO_Pin_12;
}

/* ============================================================================ */

void spi1_dma_transmit(uint8_t *buffer_addr)
{
        // while (DMA_GetFlagStatus( DMA1_FLAG_TC7) == RESET);
        // DMA_ClearFlag(DMA1_FLAG_TC7);
        // DMA_ClearFlag(DMA1_FLAG_GL7);
        // DMA_ClearFlag(DMA1_FLAG_TE7);

        DMA_DeInit(DMA1_Channel3);
        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI1->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralDST,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };
  
        DMA_Init(DMA1_Channel3, &DMA_InitStructure);
        DMA_Cmd(DMA1_Channel3, ENABLE);
        // GPIOC->ODR ^= GPIO_Pin_8;
    
        /* NVIC Initialization */
        NVIC_InitTypeDef NVIC_InitStruct = {
          .NVIC_IRQChannel = DMA1_Ch2_3_DMA2_Ch1_2_IRQn,
          .NVIC_IRQChannelPriority = 0,
          .NVIC_IRQChannelCmd = ENABLE
        };
        NVIC_Init(&NVIC_InitStruct);

        DMA_ITConfig(DMA1_Channel3, DMA1_IT_TC3, ENABLE);
        SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Tx, ENABLE);
        // GPIOC->ODR ^= GPIO_Pin_8;
}

/* ============================================================================ */

void spi2_dma_transmit(uint8_t *buffer_addr)
{
        // while (DMA_GetFlagStatus( DMA1_FLAG_TC5) == RESET);
        // DMA_ClearFlag(DMA1_FLAG_TC5);
        // DMA_ClearFlag(DMA1_FLAG_GL5);
        // DMA_ClearFlag(DMA1_FLAG_TE5);

        DMA_DeInit(DMA1_Channel5);
        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI2->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralDST,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };
  
        DMA_Init(DMA1_Channel5, &DMA_InitStructure);
        DMA_Cmd(DMA1_Channel5, ENABLE);
        // GPIOC->ODR ^= GPIO_Pin_8;
    
        /* NVIC Initialization */
        NVIC_InitTypeDef NVIC_InitStruct = {
              .NVIC_IRQChannel = DMA1_Channel4_5_IRQn,
              .NVIC_IRQChannelPriority = 0x01,
              .NVIC_IRQChannelCmd = ENABLE
        };
        NVIC_Init(&NVIC_InitStruct);

        DMA_ITConfig(DMA1_Channel5, DMA1_IT_TC3, ENABLE);
        SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE);
        // GPIOC->ODR ^= GPIO_Pin_8;
}

/* ============================================================================ */

void spi1_dma_transmit_fast(uint8_t *buffer_addr)
{
        /* Disable DMA1_Channel3 */
        DMA1_Channel3->CCR &= (uint16_t)(~DMA_CCR_EN);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI1->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralDST,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };  
        DMA_Init(DMA1_Channel3, &DMA_InitStructure);

        /* Enable DMA1_Channel3 */
        DMA1_Channel3->CCR |= DMA_CCR_EN;

        /* Enable SPI DMA Tx Request */
        SPI1->CR2 |= SPI_I2S_DMAReq_Tx;
}

/* ============================================================================ */

void spi2_dma_transmit_fast(uint8_t *buffer_addr)
{
        /* Disable DMA1_Channel5 */
        DMA1_Channel5->CCR &= (uint16_t)(~DMA_CCR_EN);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI2->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralDST,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };  
        DMA_Init(DMA1_Channel5, &DMA_InitStructure);

        /* Enable DMA1_Channel5 */
        DMA1_Channel5->CCR |= DMA_CCR_EN;

        /* Enable SPI DMA Tx Request */
        SPI2->CR2 |= SPI_I2S_DMAReq_Tx;
}

/* ============================================================================ */

void spi1_dma_enable_receive(uint8_t *buffer_addr)
{
        DMA_DeInit(DMA1_Channel2);
        DMA_Cmd(DMA1_Channel2, DISABLE);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI1->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralSRC,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_VeryHigh,
            .DMA_M2M = DMA_M2M_Disable
        };
        DMA_Init(DMA1_Channel2, &DMA_InitStructure);
        DMA_Cmd(DMA1_Channel2, ENABLE);

        SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Rx, ENABLE);
}

/* ============================================================================ */

void spi2_dma_enable_receive(uint8_t *buffer_addr)
{
        DMA_DeInit(DMA1_Channel4);
        DMA_Cmd(DMA1_Channel4, DISABLE);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI2->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralSRC,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_VeryHigh,
            .DMA_M2M = DMA_M2M_Disable
        };
        DMA_Init(DMA1_Channel4, &DMA_InitStructure);
        DMA_Cmd(DMA1_Channel4, ENABLE);

        SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Rx, ENABLE);
}

/* ============================================================================ */

void spi1_dma_enable_receive_fast(uint8_t *buffer_addr)
{
        /* Disable DMA1_Channel2 */
        DMA1_Channel2->CCR &= (uint16_t)(~DMA_CCR_EN);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI1->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralSRC,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };
        DMA_Init(DMA1_Channel2, &DMA_InitStructure);

        /* Enable SPI DMA Tx Request */
        SPI1->CR2 |= SPI_I2S_DMAReq_Rx;
        /* Enable DMA1_Channel2 */
        DMA1_Channel2->CCR |= DMA_CCR_EN;
}

/* ============================================================================ */

void spi2_dma_enable_receive_fast(uint8_t *buffer_addr)
{
        /* Disable DMA1_Channel4 */
        DMA1_Channel4->CCR &= (uint16_t)(~DMA_CCR_EN);

        DMA_InitTypeDef DMA_InitStructure = {
        /* Configure DMA Initialization Structure */
            .DMA_PeripheralBaseAddr = (uint32_t)(&(SPI2->DR)),
            .DMA_MemoryBaseAddr = (uint32_t)buffer_addr,
            .DMA_DIR = DMA_DIR_PeripheralSRC,
            .DMA_BufferSize =  (uint32_t)3,                     // data number (bytes) that wanna transfer
            .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
            .DMA_MemoryInc = DMA_MemoryInc_Enable,
            .DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte,
            .DMA_MemoryDataSize = DMA_MemoryDataSize_Byte,
            .DMA_Mode = DMA_Mode_Normal,
            .DMA_Priority = DMA_Priority_Medium,
            .DMA_M2M = DMA_M2M_Disable
        };
        DMA_Init(DMA1_Channel4, &DMA_InitStructure);

        /* Enable SPI DMA Tx Request */
        SPI2->CR2 |= SPI_I2S_DMAReq_Rx;
        /* Enable DMA1_Channel2 */
        DMA1_Channel4->CCR |= DMA_CCR_EN;
}
/* ============================================================================ */

uint8_t spi1_buffer[25];
uint8_t uart_from_spi1_buffer[25];
uint8_t dma_spi1_complete_flag =0;
uint8_t spi1_RequestAddr[]= {0xf5, 0xf5, 0xf5}; // 0x75 (WHOAMI of MPU6500) in read mode
// uint8_t testspi[]= {0xBB, 0xBC, 0x00}; // reading ACCEL_XOUT_H and L byte of MPU6500

void DMA1_Channel2_3_IRQHandler(void)
{
        if( DMA_GetITStatus(DMA1_IT_TC3) == SET){   // trans.
            DMA_ClearITPendingBit(DMA1_IT_TC3);
            // GPIOC->ODR ^= GPIO_Pin_8;
        }

        if(DMA_GetITStatus(DMA1_IT_TC2) == SET){
            DMA_ClearITPendingBit(DMA1_IT_TC2);       // rcv.
        GPIOC->ODR ^= GPIO_Pin_7;                            // nCS 

        for(int i = 0;i<3;i++)
            uart_from_spi1_buffer[i] = spi1_buffer[i];

        dma_spi1_complete_flag=1;

        /* Re-transmission */
            spi1_Trigger_transmission(spi1_RequestAddr);
        };
}

/* ============================================================================ */

uint8_t spi2_buffer[25];
uint8_t uart_from_spi2_buffer[25];
uint8_t dma_spi2_complete_flag =0;
// uint8_t spi2_RequestAddr[]= {0xf5, 0xf5, 0xf5}; // 0x75 (WHOAMI of MPU6500) in read mode
uint8_t spi2_RequestAddr[]= {0xBB, 0xBC, 0x00}; // reading ACCEL_XOUT_H and L byte of MPU6500
uint16_t IMUData = 0;

void DMA1_Channel4_5_6_7_IRQHandler(void)
{
        if( DMA_GetITStatus(DMA1_IT_TC5) == SET){   // trans.
            DMA_ClearITPendingBit(DMA1_IT_TC5);
            // GPIOB->ODR ^= GPIO_Pin_12;
        }

        if(DMA_GetITStatus(DMA1_IT_TC4) == SET)
        {
            DMA_ClearITPendingBit(DMA1_IT_TC4);       // rcv.
            GPIOB->ODR ^= GPIO_Pin_12;                      // nCS 

            for(int i = 0;i<3;i++)
            {
                uart_from_spi2_buffer[i] = spi2_buffer[i];
            };

            dma_spi2_complete_flag=1;

            IMUData = spi2_buffer[1];
            IMUData = (IMUData<<8) | spi2_buffer[2];
            GPIOC -> ODR ^= GPIO_Pin_9;

            /* Re-transmission */
            spi2_GetIMUData(spi2_RequestAddr);
        };
}

/* ============================================================================ */

void spi1_Trigger_transmission(uint8_t *buffer_addr)
{
            spi1_dma_enable_receive_fast(spi1_buffer);
            spi1_dma_transmit_fast(buffer_addr);
            GPIOC->ODR ^= GPIO_Pin_7;       // nCS
}

/* ============================================================================ */

void spi2_GetIMUData(uint8_t *buffer_addr)
{
            spi2_dma_enable_receive_fast(spi2_buffer);
            spi2_dma_transmit_fast(buffer_addr);
            GPIOB->ODR ^= GPIO_Pin_12;     // nCS
}